﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using PicDiff.Core;

namespace PicDiff
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main(string[] args)
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);


      WorkingSet ws = null;
      try
      {
        if (args.Length < 2) // for testing purposes
          ws = new WorkingSet(@"d:\tmp\diff\landa.png", @"d:\tmp\diff\landb.png");
        else
          ws = CreateWorkingSet(args[0], args[1]);        

      }
      catch (ArgumentException argEx)
      {
        MessageBox.Show(argEx.Message + " doesn't exist.");
      }
      catch (Exception)
      {
        MessageBox.Show("Valid image files must be specified");
      }

	//		var ws = new WorkingSet(@"/home/zdeslav/dev/PicDiff/diff/landA.png", 
	//		                        @"/home/zdeslav/dev/PicDiff/diff/landB.png");
      var frm = new Form1(ws);
      Application.Run(frm);
    }

    private static WorkingSet CreateWorkingSet(string path1, string path2)
    {
      if (!File.Exists(path1)) throw new ArgumentException(path1);
      if (!File.Exists(path2)) throw new ArgumentException(path2);
      return new WorkingSet(path1, path2);
    }
  }
}
