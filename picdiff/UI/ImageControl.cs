﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using PicDiff.Core;

namespace PicDiff
{
  public partial class ImageControl : ScrollableControl
  {
    [DllImport("User32.dll")]
    private static extern short GetAsyncKeyState(Keys vKey);

    public delegate void ZoomChangedDelegate(int zoom);

    public event ZoomChangedDelegate ZoomChanged;

    private Tool m_tool = new NullTool();
    private int m_zoom = 100;
    private Point m_scrollPos;

    public ImageControl()
    {
      InitializeComponent();
      BackColor = Color.Lavender;
      DoubleBuffered = true;
      AutoScroll = true;
    }

    protected override void OnPaint(PaintEventArgs pe)
    {
      base.OnPaint(pe);
#if DEBUG
      System.Diagnostics.Stopwatch sw = new Stopwatch();
      sw.Start();
#endif

      pe.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
      pe.Graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
      var size = m_tool.ResultSize;

      var w_client = Width - (GetScrollState(ScrollStateVScrollVisible) ? SystemInformation.VerticalScrollBarWidth : 0);
      var h_client = Height - (GetScrollState(ScrollStateHScrollVisible) ? SystemInformation.HorizontalScrollBarHeight : 0);

      float scale = m_zoom / 100.0f;
      var x_off = m_scrollPos.X + 5 * Math.Sign(m_scrollPos.X);
      var y_off = m_scrollPos.Y - 5 * Math.Sign(m_scrollPos.Y);

      // todo: use transformation instead of manual
      pe.Graphics.TranslateTransform(x_off +  (w_client - size.Width * scale) / 2, y_off + (h_client - size.Height * scale) / 2);
      pe.Graphics.ScaleTransform(scale, scale);
      m_tool.Render(pe.Graphics);

#if DEBUG
      sw.Stop();
      pe.Graphics.ResetTransform();
      var msg = string.Format(
        "rendering time: {0} ms, resultSize: {2}x{3}, clientRc: {4}x{5}, autoScrollPos: {6}x{7}, manualScrollPos: {8}x{9}, x_off: {10}", 
        sw.ElapsedMilliseconds, m_zoom, 
        size.Width, size.Height, 
        ClientRectangle.Width, ClientRectangle.Height, 
        AutoScrollPosition.X, AutoScrollPosition.Y,
        m_scrollPos.X, m_scrollPos.Y,
        x_off);
      pe.Graphics.DrawString(msg, SystemFonts.StatusFont, Brushes.Black, 4, 4);
#endif
    }

    protected override void OnSizeChanged(EventArgs e)
    {
      base.OnSizeChanged(e);
      Invalidate();
      FixScrollbars();
    }

    internal void SetTool(Tool tool)
    {
      if (tool == null) throw new ArgumentNullException("tool");
      m_tool = tool;
      Invalidate();
    }

    public void SetZoom(int zoom)
    {
      if(zoom != m_zoom)
      {
        m_zoom = zoom;
        Invalidate();
        FixScrollbars();
      }
    }

    protected override void OnMouseWheel(MouseEventArgs e)
    {
      var ctrlPressed = GetAsyncKeyState(Keys.ControlKey) < 0;

      if (ctrlPressed && ZoomChanged != null)
      {
        ZoomChanged(m_zoom + e.Delta / 12);
      }
      else
      {
        base.OnMouseWheel(e);
        Invalidate();
      }
    }

    protected override void OnScroll(ScrollEventArgs se)
    { 
      base.OnScroll(se);
      if(ScrollOrientation.HorizontalScroll == se.ScrollOrientation)
        m_scrollPos.X -= se.NewValue - se.OldValue;
      if (ScrollOrientation.VerticalScroll == se.ScrollOrientation)
        m_scrollPos.Y -= se.NewValue - se.OldValue;

      Invalidate();
    }

    void FixScrollbars()
    {
      var size = m_tool.ResultSize;
      float inc = m_zoom / 100.0f;

      float w = size.Width * inc;
      float h = size.Height * inc;

      var rc = ClientRectangle;

      rc.Width -= GetScrollState(ScrollStateVScrollVisible) ? SystemInformation.VerticalScrollBarWidth: 0;
      rc.Height -= GetScrollState(ScrollStateHScrollVisible) ? SystemInformation.HorizontalScrollBarHeight : 0;


      {
        var new_w = (int) w;
        var new_h = (int) h;
        AutoScrollMinSize = new Size(new_w, new_h);

        var w_pos = w > rc.Width ? (new_w - rc.Width) / 2 : 0;
        var h_pos = h > rc.Height ? (new_h - rc.Height) / 2 : 0;
        AutoScrollPosition = new Point(w_pos, h_pos);
        m_scrollPos.X = m_scrollPos.Y = 0;
      }
    }
  }
}

