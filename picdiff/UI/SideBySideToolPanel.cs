using System;
using PicDiff.Core;

namespace PicDiff
{
  public class SideBySideToolPanel : SideBySidePanelBase
  {
    private System.Windows.Forms.ToolStrip toolStrip1;
    private System.Windows.Forms.ToolStripButton toolStripButton1;
    private System.Windows.Forms.ToolStripButton toolStripButton2;

    public SideBySideToolPanel()
    {
      InitializeComponent();
    }

    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SideBySideToolPanel));
      this.toolStrip1 = new System.Windows.Forms.ToolStrip();
      this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
      this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
      this.toolStrip1.SuspendLayout();
      this.SuspendLayout();
      // 
      // toolStrip1
      // 
      this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
      this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
      this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
      this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2});
      this.toolStrip1.Location = new System.Drawing.Point(0, 0);
      this.toolStrip1.Name = "toolStrip1";
      this.toolStrip1.Size = new System.Drawing.Size(106, 54);
      this.toolStrip1.TabIndex = 0;
      this.toolStrip1.Text = "toolStrip1";
      // 
      // toolStripButton1
      // 
      this.toolStripButton1.Image = ((System.Drawing.Image) (resources.GetObject("toolStripButton1.Image")));
      this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.toolStripButton1.Name = "toolStripButton1";
      this.toolStripButton1.Size = new System.Drawing.Size(36, 51);
      this.toolStripButton1.Text = "Hor";
      this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
      this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
      // 
      // toolStripButton2
      // 
      this.toolStripButton2.Image = ((System.Drawing.Image) (resources.GetObject("toolStripButton2.Image")));
      this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.toolStripButton2.Name = "toolStripButton2";
      this.toolStripButton2.Size = new System.Drawing.Size(36, 51);
      this.toolStripButton2.Text = "Vert";
      this.toolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
      this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
      // 
      // SideBySideToolPanel
      // 
      this.Controls.Add(this.toolStrip1);
      this.Name = "SideBySideToolPanel";
      this.Size = new System.Drawing.Size(180, 60);
      this.toolStrip1.ResumeLayout(false);
      this.toolStrip1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

      toolStripButton1.Checked = m_params.Orientation == SideBySideOrientation.Horizontal;
      toolStripButton2.Checked = m_params.Orientation == SideBySideOrientation.Vertical;
    }

    private void toolStripButton1_Click(object sender, System.EventArgs e)
    {
      m_params.Orientation = SideBySideOrientation.Horizontal;
      ApplyChanges();
    }

    private void toolStripButton2_Click(object sender, System.EventArgs e)
    {
      m_params.Orientation = SideBySideOrientation.Vertical;
      ApplyChanges();
    }

    private void ApplyChanges()
    {
      toolStripButton1.Checked = m_params.Orientation == SideBySideOrientation.Horizontal;
      toolStripButton2.Checked = m_params.Orientation == SideBySideOrientation.Vertical;
      OnParametersChanged();
    }
  }
  public class SideBySidePanelBase : ToolPanel<SideBySideToolArgs> { }

}