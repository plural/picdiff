using System;
using PicDiff.Core;

namespace PicDiff
{
  public class DifferenceToolPanel : DifferenceToolPanelBase
  {
    private System.Windows.Forms.CheckBox checkBox1;
 //   private MyTrackBar myTrackBar1;
    private System.Windows.Forms.ComboBox comboBox1;

    public DifferenceToolPanel()
    {
      InitializeComponent();

      if (!DesignMode)
      {
        foreach (var value in Enum.GetValues(typeof(DifferenceMode)))
        {
          comboBox1.Items.Add((DifferenceMode)value);  
        }
        
        comboBox1.SelectedItem = m_params.Mode;

        comboBox1.SelectedIndexChanged += new System.EventHandler(this.ModeChanged);
      }
    }

    protected override void OnSizeChanged(System.EventArgs e)
    {
      base.OnSizeChanged(e);
      var newTop = (Height - (comboBox1.Height + checkBox1.Height + (checkBox1.Top - comboBox1.Bottom)))/2;
      checkBox1.Top -= (comboBox1.Top - newTop);
      comboBox1.Top = newTop;
    }
 
    private void InitializeComponent()
    {
      this.comboBox1 = new System.Windows.Forms.ComboBox();
      this.checkBox1 = new System.Windows.Forms.CheckBox();
      this.SuspendLayout();
      // 
      // comboBox1
      // 
      this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.Location = new System.Drawing.Point(3, 7);
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.Size = new System.Drawing.Size(175, 21);
      this.comboBox1.TabIndex = 0;
      // 
      // checkBox1
      // 
      this.checkBox1.AutoSize = true;
      this.checkBox1.Location = new System.Drawing.Point(4, 32);
      this.checkBox1.Name = "checkBox1";
      this.checkBox1.Size = new System.Drawing.Size(144, 17);
      this.checkBox1.TabIndex = 1;
      this.checkBox1.Text = "Transparent Background";
      this.checkBox1.UseVisualStyleBackColor = true;
      this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
      // 
      // DifferenceToolPanel
      // 
      this.BackColor = System.Drawing.Color.Transparent;
      this.Controls.Add(this.checkBox1);
      this.Controls.Add(this.comboBox1);
      this.Name = "DifferenceToolPanel";
      this.Size = new System.Drawing.Size(211, 73);
      this.ResumeLayout(false);
      this.PerformLayout();

    }


    private void ModeChanged(object sender, System.EventArgs e)
    {
      m_params.Mode = (DifferenceMode) comboBox1.SelectedItem;
      OnParametersChanged();
    }

    private void checkBox1_CheckedChanged(object sender, System.EventArgs e)
    {
      m_params.TransparentBackground = checkBox1.Checked;
      OnParametersChanged();
    }
  }

  public class DifferenceToolPanelBase : ToolPanel<DifferenceToolArgs> { }

}