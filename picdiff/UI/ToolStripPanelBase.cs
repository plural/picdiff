using System.Windows.Forms;
using System.Windows.Forms.Design;
using PicDiff.Core;

namespace PicDiff
{
  public class ToolStripPanelBase<T> : ToolStripControlHost where T : UserControl, new()
  {
    public ToolStripPanelBase() : base(new T()) {}

    public T Panel { get { return (T)Control; } }
  }

  [ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip)]
  public class ToolStripOverlayPanel : ToolStripPanelBase<OverlayToolPanel>
  { }

  [ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip)]
  public class ToolStripSplitPanel : ToolStripPanelBase<SplitToolPanel>
  { }

  [ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip)]
  public class ToolStripSideBySidePanel : ToolStripPanelBase<SideBySideToolPanel>
  { }

  [ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip)]
  public class ToolStripDifferencePanel : ToolStripPanelBase<DifferenceToolPanel>
  { }

}