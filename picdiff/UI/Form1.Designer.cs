﻿namespace PicDiff
{
  partial class Form1
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
      this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
      this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
      this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
      this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
      this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
      this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
      this.toolStripTraceBarItem1 = new PicDiff.ToolStripTraceBarItem();
      this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
      this.toolStrip1 = new System.Windows.Forms.ToolStrip();
      this.toolStripSideBySidePanel1 = new PicDiff.ToolStripSideBySidePanel();
      this.toolStripOverlayPanel1 = new PicDiff.ToolStripOverlayPanel();
      this.toolStripSplitPanel1 = new PicDiff.ToolStripSplitPanel();
      this.toolStripDifferencePanel1 = new PicDiff.ToolStripDifferencePanel();
      this.statusStrip1 = new System.Windows.Forms.StatusStrip();
      this.lblInfo = new System.Windows.Forms.ToolStripStatusLabel();
      this.lblLast = new System.Windows.Forms.ToolStripStatusLabel();
      this.imageControl1 = new PicDiff.ImageControl();
      this.toolStrip1.SuspendLayout();
      this.statusStrip1.SuspendLayout();
      this.SuspendLayout();
      // 
      // toolStripButton1
      // 
      this.toolStripButton1.Image = ((System.Drawing.Image) (resources.GetObject("toolStripButton1.Image")));
      this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.toolStripButton1.Name = "toolStripButton1";
      this.toolStripButton1.Size = new System.Drawing.Size(68, 50);
      this.toolStripButton1.Text = "SideBySide";
      this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
      // 
      // toolStripButton2
      // 
      this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.toolStripButton2.Name = "toolStripButton2";
      this.toolStripButton2.Size = new System.Drawing.Size(51, 50);
      this.toolStripButton2.Text = "Overlay";
      this.toolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
      // 
      // toolStripButton3
      // 
      this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.toolStripButton3.Name = "toolStripButton3";
      this.toolStripButton3.Size = new System.Drawing.Size(34, 50);
      this.toolStripButton3.Text = "Split";
      this.toolStripButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
      // 
      // toolStripButton4
      // 
      this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.toolStripButton4.Name = "toolStripButton4";
      this.toolStripButton4.Size = new System.Drawing.Size(65, 50);
      this.toolStripButton4.Text = "Difference";
      this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
      // 
      // toolStripSeparator1
      // 
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      this.toolStripSeparator1.Size = new System.Drawing.Size(6, 53);
      // 
      // toolStripButton6
      // 
      this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.toolStripButton6.Name = "toolStripButton6";
      this.toolStripButton6.Size = new System.Drawing.Size(85, 50);
      this.toolStripButton6.Text = "Fit to Window";
      this.toolStripButton6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
      this.toolStripButton6.Click += new System.EventHandler(this.toolStripButton6_Click);
      // 
      // toolStripButton5
      // 
      this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.toolStripButton5.Name = "toolStripButton5";
      this.toolStripButton5.Size = new System.Drawing.Size(42, 50);
      this.toolStripButton5.Text = "100 %";
      this.toolStripButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
      // 
      // toolStripTraceBarItem1
      // 
      this.toolStripTraceBarItem1.CurrentZoom = 100;
      this.toolStripTraceBarItem1.Margin = new System.Windows.Forms.Padding(0, 5, 0, 2);
      this.toolStripTraceBarItem1.Name = "toolStripTraceBarItem1";
      this.toolStripTraceBarItem1.Size = new System.Drawing.Size(104, 46);
      this.toolStripTraceBarItem1.Text = "toolStripTraceBarItem1";
      // 
      // toolStripSeparator2
      // 
      this.toolStripSeparator2.Name = "toolStripSeparator2";
      this.toolStripSeparator2.Size = new System.Drawing.Size(6, 53);
      // 
      // toolStrip1
      // 
      this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
      this.toolStrip1.ImageScalingSize = new System.Drawing.Size(45, 31);
      this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton3,
            this.toolStripButton4,
            this.toolStripSeparator1,
            this.toolStripButton6,
            this.toolStripButton5,
            this.toolStripTraceBarItem1,
            this.toolStripSeparator2,
            this.toolStripSideBySidePanel1,
            this.toolStripOverlayPanel1,
            this.toolStripSplitPanel1,
            this.toolStripDifferencePanel1});
      this.toolStrip1.Location = new System.Drawing.Point(0, 0);
      this.toolStrip1.Name = "toolStrip1";
      this.toolStrip1.Size = new System.Drawing.Size(1071, 53);
      this.toolStrip1.TabIndex = 0;
      this.toolStrip1.Text = "toolStrip1";
      // 
      // toolStripSideBySidePanel1
      // 
      this.toolStripSideBySidePanel1.Name = "toolStripSideBySidePanel1";
      this.toolStripSideBySidePanel1.Size = new System.Drawing.Size(75, 54);
      this.toolStripSideBySidePanel1.Text = "toolStripSideBySidePanel1";
      this.toolStripSideBySidePanel1.Visible = false;
      // 
      // toolStripOverlayPanel1
      // 
      this.toolStripOverlayPanel1.BackColor = System.Drawing.Color.Transparent;
      this.toolStripOverlayPanel1.Name = "toolStripOverlayPanel1";
      this.toolStripOverlayPanel1.Size = new System.Drawing.Size(140, 51);
      this.toolStripOverlayPanel1.Text = "toolStripOverlayPanel1";
      this.toolStripOverlayPanel1.Visible = false;
      // 
      // toolStripSplitPanel1
      // 
      this.toolStripSplitPanel1.BackColor = System.Drawing.Color.Transparent;
      this.toolStripSplitPanel1.Name = "toolStripSplitPanel1";
      this.toolStripSplitPanel1.Size = new System.Drawing.Size(140, 51);
      this.toolStripSplitPanel1.Text = "toolStripSplitPanel1";
      this.toolStripSplitPanel1.Visible = false;
      // 
      // toolStripDifferencePanel1
      // 
      this.toolStripDifferencePanel1.BackColor = System.Drawing.Color.Transparent;
      this.toolStripDifferencePanel1.Name = "toolStripDifferencePanel1";
      this.toolStripDifferencePanel1.Size = new System.Drawing.Size(181, 50);
      this.toolStripDifferencePanel1.Text = "toolStripDifferencePanel1";
      this.toolStripDifferencePanel1.Visible = false;
      // 
      // statusStrip1
      // 
      this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblInfo,
            this.lblLast});
      this.statusStrip1.Location = new System.Drawing.Point(0, 461);
      this.statusStrip1.Name = "statusStrip1";
      this.statusStrip1.Size = new System.Drawing.Size(1071, 22);
      this.statusStrip1.TabIndex = 1;
      this.statusStrip1.Text = "statusStrip1";
      // 
      // lblInfo
      // 
      this.lblInfo.Name = "lblInfo";
      this.lblInfo.Size = new System.Drawing.Size(118, 17);
      this.lblInfo.Text = "toolStripStatusLabel1";
      // 
      // lblLast
      // 
      this.lblLast.Name = "lblLast";
      this.lblLast.Size = new System.Drawing.Size(938, 17);
      this.lblLast.Spring = true;
      this.lblLast.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // imageControl1
      // 
      this.imageControl1.AutoScroll = true;
      this.imageControl1.AutoScrollMinSize = new System.Drawing.Size(100, 100);
      this.imageControl1.BackColor = System.Drawing.SystemColors.AppWorkspace;
      this.imageControl1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.imageControl1.Location = new System.Drawing.Point(0, 53);
      this.imageControl1.Name = "imageControl1";
      this.imageControl1.Size = new System.Drawing.Size(1071, 408);
      this.imageControl1.TabIndex = 2;
      this.imageControl1.Text = "imageControl1";
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1071, 483);
      this.Controls.Add(this.imageControl1);
      this.Controls.Add(this.statusStrip1);
      this.Controls.Add(this.toolStrip1);
      this.DoubleBuffered = true;
      this.Icon = ((System.Drawing.Icon) (resources.GetObject("$this.Icon")));
      this.Name = "Form1";
      this.Text = "PicDiff";
      this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
      this.toolStrip1.ResumeLayout(false);
      this.toolStrip1.PerformLayout();
      this.statusStrip1.ResumeLayout(false);
      this.statusStrip1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private ImageControl imageControl1;
    private System.Windows.Forms.ToolStripButton toolStripButton1;
    private System.Windows.Forms.ToolStripButton toolStripButton2;
    private System.Windows.Forms.ToolStripButton toolStripButton3;
    private System.Windows.Forms.ToolStripButton toolStripButton4;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private System.Windows.Forms.ToolStripButton toolStripButton6;
    private System.Windows.Forms.ToolStripButton toolStripButton5;
    private ToolStripTraceBarItem toolStripTraceBarItem1;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.ToolStrip toolStrip1;
    private ToolStripSideBySidePanel toolStripSideBySidePanel1;
    private ToolStripOverlayPanel toolStripOverlayPanel1;
    private ToolStripSplitPanel toolStripSplitPanel1;
    private ToolStripDifferencePanel toolStripDifferencePanel1;
    private System.Windows.Forms.StatusStrip statusStrip1;
    private System.Windows.Forms.ToolStripStatusLabel lblInfo;
    private System.Windows.Forms.ToolStripStatusLabel lblLast;
  }
}

