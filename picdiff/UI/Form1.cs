﻿using System;
using System.Windows.Forms;
using PicDiff.Core;

namespace PicDiff
{
  public partial class Form1 : Form
  {
    private readonly WorkingSet m_workingSet;
    private Tool m_tool;
    private int m_zoom = 100;
    public Form1(WorkingSet set)
    {
      m_workingSet = set;
      //m_tool = new SideBySideTool(set);
      //FormBorderStyle = FormBorderStyle.None;
      InitializeComponent();

      toolStripTraceBarItem1.ZoomChanged += (s, a) => SetZoom(toolStripTraceBarItem1.CurrentZoom);
      toolStripButton1.Click += (s, a) => SwitchTool(new SideBySideTool(m_workingSet));
      toolStripButton2.Click += (s, a) => SwitchTool(new OverlayTool(m_workingSet));
      toolStripButton3.Click += (s, a) => SwitchTool(new SplitTool(m_workingSet));
      toolStripButton4.Click += (s, a) => SwitchTool(new DifferenceTool(m_workingSet));
      toolStripButton5.Click += (s, a) => SetZoom(100);
      imageControl1.ZoomChanged += z => SetZoom(z);

      UpdateWorkingSetInfo(set);
      SetZoom(m_zoom);
      SwitchTool(new SideBySideTool(set));
    }

    private void UpdateWorkingSetInfo(WorkingSet set)
    {
      var info = string.Format("{0}     |     {1}", set.ImageA, set.ImageB);
      lblInfo.Text = info;
    }

    private void SwitchTool(Tool tool)
    {
      toolStripSideBySidePanel1.Visible = tool is SideBySideTool;
      toolStripOverlayPanel1.Visible = tool is OverlayTool;
      toolStripSplitPanel1.Visible = tool is SplitTool;
      toolStripDifferencePanel1.Visible = tool is DifferenceTool;

      toolStripButton1.Checked = tool is SideBySideTool;
      toolStripButton2.Checked = tool is OverlayTool;
      toolStripButton3.Checked = tool is SplitTool;
      toolStripButton4.Checked = tool is DifferenceTool;

      if(tool is SideBySideTool)
      {
        toolStripSideBySidePanel1.Panel.Initialize((SideBySideTool)tool);
      }

      if (tool is OverlayTool)
      {
        toolStripOverlayPanel1.Panel.Initialize((OverlayTool)tool);
      }

      if (tool is SplitTool)
      {
        toolStripSplitPanel1.Panel.Initialize((SplitTool) tool);
      }

      if (tool is DifferenceTool)
      {
        toolStripDifferencePanel1.Panel.Initialize((DifferenceTool) tool);
      }

      m_tool = tool;
      imageControl1.SetTool(m_tool);
    }

    private void SetZoom(int zoom)
    {
      if (zoom < 25) zoom = 25;
      if (zoom > 500) zoom = 500;                            

      lblLast.Text = "Zoom: " + zoom + " %";
      imageControl1.SetZoom(zoom);
      toolStripTraceBarItem1.CurrentZoom = zoom;
    }

    private void toolStripButton6_Click(object sender, EventArgs e)
    {
      var winSize = imageControl1.Size;
      var imgSize = m_tool.ResultSize;

      var zoom = 100 * Math.Min((winSize.Height - 20) / (float) imgSize.Height, (winSize.Width - 20) / (float) imgSize.Width);
      zoom = Math.Max(25, zoom);
      zoom = Math.Min(500, zoom);
      SetZoom((int)zoom);
    }
  }
}
