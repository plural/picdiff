using PicDiff.Core;

namespace PicDiff
{
  // SplitToolPanelBase - workaround for GUI editor unable to work with generic base class
  public class SplitToolPanel : SplitToolPanelBase  
  {
    private MyTrackBar myTrackBar1;

    public SplitToolPanel()
    {
      InitializeComponent();
    }


    protected override void OnSizeChanged(System.EventArgs e)
    {
      base.OnSizeChanged(e);
      myTrackBar1.Top = (Height - myTrackBar1.Height) / 2;
    }


    private void InitializeComponent()
    {
      this.myTrackBar1 = new PicDiff.MyTrackBar();
      ((System.ComponentModel.ISupportInitialize) (this.myTrackBar1)).BeginInit();
      this.SuspendLayout();
      // 
      // myTrackBar1
      // 
      this.myTrackBar1.Location = new System.Drawing.Point(9, 12);
      this.myTrackBar1.Maximum = 100;
      this.myTrackBar1.Name = "myTrackBar1";
      this.myTrackBar1.Size = new System.Drawing.Size(128, 45);
      this.myTrackBar1.TabIndex = 0;
      this.myTrackBar1.TickFrequency = 10;
      this.myTrackBar1.Value = (int)m_params.Ratio;
      this.myTrackBar1.TickStyle = System.Windows.Forms.TickStyle.Both;
      this.myTrackBar1.ValueChanged += new System.EventHandler(this.myTrackBar1_ValueChanged);
      // 
      // SplitToolPanel
      // 
      this.BackColor = System.Drawing.Color.Transparent;
      this.Controls.Add(this.myTrackBar1);
      this.Name = "SplitToolPanel";
      this.Size = new System.Drawing.Size(150, 71);
      ((System.ComponentModel.ISupportInitialize) (this.myTrackBar1)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    private void myTrackBar1_ValueChanged(object sender, System.EventArgs e)
    {
      m_params.Ratio = (uint)myTrackBar1.Value;
      OnParametersChanged();
    }
  }

  public class SplitToolPanelBase : ToolPanel<SplitToolArgs>{}
}