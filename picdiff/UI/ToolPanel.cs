using System;
using System.Windows.Forms;
using PicDiff.Core;

namespace PicDiff
{
  public class ToolPanel<Params> : UserControl where Params : new()
  {
    private BmpBackedTool<Params> m_tool;
    protected Params m_params = new Params();

    public event EventHandler ParametersChanged;

    public void Initialize(BmpBackedTool<Params> tool)
    {
      m_tool = tool;
    }

    protected void OnParametersChanged()
    {
      m_tool.ApplyParameters(m_params);
      if (ParametersChanged != null)
        ParametersChanged(this, EventArgs.Empty);

      ParentForm.Invalidate(true);

    }
  }
}