using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using PicDiff.Core;

namespace PicDiff
{
  [ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip)]
  public class ToolStripTraceBarItem : ToolStripControlHost
  {
    public event EventHandler ZoomChanged;
    private TrackBar m_trackbar;

    public ToolStripTraceBarItem() : base(new TrackBar())
    {
      m_trackbar = Control as TrackBar;
      m_trackbar.SetRange(25, 500);
      m_trackbar.Value = 100;
      m_trackbar.TickStyle = TickStyle.Both;
      m_trackbar.TickFrequency = 1000;
      if(!DesignMode)
      {
        m_trackbar.ValueChanged += (s, a) => { if (ZoomChanged != null) ZoomChanged(this, EventArgs.Empty); };
      }
    }

    public int CurrentZoom
    {
      get { return m_trackbar.Value; }
      set { if(m_trackbar != null) m_trackbar.Value = value; }
    }
  }

  // http://www.codeproject.com/Articles/35104/gTrackBar-A-Custom-TrackBar-UserControl-VB-NET
  public class MyTrackBar : TrackBar
  {
    protected override void OnPaintBackground(PaintEventArgs pevent)
    {
     // base.OnPaintBackground(pevent);
    }
    protected override void OnPaint(PaintEventArgs e)
    {
    //  base.OnPaint(e);
    } 
  }
}