using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace PicDiff.Core
{
  class NullTool : Tool
  {
    public NullTool() : base(null)
    {
    }

    public override void Render(Graphics graphics)
    {
      graphics.FillRectangle(Brushes.Blue, new Rectangle(new Point(0, 0), ResultSize));
    }

    public override Size ResultSize
    {
      get { return new Size(100, 100);}
    }
  }
}