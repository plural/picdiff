using System;
using System.Drawing.Imaging;

namespace PicDiff.Core
{
  static class Helpers
  {
    public static byte GetAlpha(this uint rgb)
    {
      return (byte)((rgb & 0xFF000000) >> 24);
    }
    public static byte GetR(this uint rgb)
    {
      return (byte) ((rgb & 0xFF0000) >> 16);
    }
    public static byte GetG(this uint rgb)
    {
      return (byte) ((rgb & 0xFF00) >> 8);
    }
    public static byte GetB(this uint rgb)
    {
      return (byte) (rgb & 0xFF);
    }

    public static uint ToArgb(uint a, uint r, uint g, uint b)
    {
      return (a << 24 | r << 16 | g << 8 | b);
    }

    public static uint Mix(uint c1, uint c2, uint ratio)
    {
      float weightA = (100f - ratio) / 100.0f;
      float weightB = 1 - weightA;

      
      var a = c1.GetAlpha() * weightA + c2.GetAlpha() * weightB;
      var r = c1.GetR() * weightA + c2.GetR() * weightB;
      var g = c1.GetG() * weightA + c2.GetG() * weightB;
      var b = c1.GetB() * weightA + c2.GetB() * weightB;

      return ToArgb((byte) a, (byte) r, (byte) g, (byte) b);
    }
    public static float ToKilobytes(this long val)
    {
      return val/1024f;
    }

    public static string Humanize(this PixelFormat fmt)
    {
      switch (fmt)
      {
        case PixelFormat.Format1bppIndexed:
          return "1 bpp palette";
        case PixelFormat.Format4bppIndexed:
          return "4 bpp palette";
        case PixelFormat.Format8bppIndexed:
          return "8 bpp palette";
        case PixelFormat.Format16bppGrayScale:
          return "16 bpp grayscale";
        case PixelFormat.Format16bppRgb555:
          return "16 bpp RGB [555]";
        case PixelFormat.Format16bppRgb565:
          return "16 bpp RGB [565]";
        case PixelFormat.Format16bppArgb1555:
          return "16 bpp RGB [1555]";
        case PixelFormat.Format24bppRgb:
          return "24 bpp RGB";
        case PixelFormat.Format32bppRgb:
          return "32 bpp RGB";
        case PixelFormat.Canonical:
        case PixelFormat.Format32bppArgb:
          return "32 bpp ARGB";
        case PixelFormat.Format32bppPArgb:
          return "32 bpp ARGB - premultiplied";
        case PixelFormat.Format48bppRgb:
          return "48 bpp RGB";
        case PixelFormat.Format64bppArgb:
          return "48 bpp ARGB";
        case PixelFormat.Format64bppPArgb:
          return "48 bpp ARGB - premultiplied";
        case PixelFormat.Max:
          throw new ArgumentOutOfRangeException("fmt");
        default:
          return fmt.ToString();
      }
    }                           
  }
}