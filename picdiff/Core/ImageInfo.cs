using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace PicDiff.Core
{
  public class ImageInfo
  {
    public string Path { get; set; }
    public Bitmap Bmp { get; set; }
    public PixelFormat OriginalFormat { get; set; }
    public long FileSize { get; set; }

    public ImageInfo(string path)
    {
      Path = path;
      var img = Image.FromFile(Path);
      Bmp = new Bitmap(img);
      OriginalFormat = img.PixelFormat;
      FileSize = new FileInfo(Path).Length;
    }

    public override string ToString()
    {
      return string.Format("{0}: {1} x {2} x {3}, {4:N2} kB",
              Path, Bmp.Width, Bmp.Height, OriginalFormat.Humanize(), FileSize.ToKilobytes());
    }
  }
}