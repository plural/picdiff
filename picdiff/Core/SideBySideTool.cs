using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace PicDiff.Core
{
  class SideBySideTool : BmpBackedTool<SideBySideToolArgs>
  {
    const int SPACER = 10;

    public SideBySideTool(WorkingSet workingSet) : base(workingSet) { }

    protected override void UpdateBackingImage(SideBySideToolArgs parameters)
    {
      int currentSpace = SPACER;

      var w = WorkingSet.ImageA.Bmp.Width + WorkingSet.ImageB.Bmp.Width + currentSpace;
      var h = Math.Max(WorkingSet.ImageA.Bmp.Height, WorkingSet.ImageB.Bmp.Height);
      if(Parameters.Orientation == SideBySideOrientation.Horizontal)
      {
        h = WorkingSet.ImageA.Bmp.Height + WorkingSet.ImageB.Bmp.Height + currentSpace;
        w = Math.Max(WorkingSet.ImageA.Bmp.Width, WorkingSet.ImageB.Bmp.Width);
      }

      m_mixedBmp = new Bitmap(w, h, PixelFormat.Format32bppArgb);

      using (var g = Graphics.FromImage(m_mixedBmp))
      {
        g.DrawImageUnscaled(WorkingSet.ImageA.Bmp, 0, 0);

        if (Parameters.Orientation == SideBySideOrientation.Horizontal)
          g.DrawImageUnscaled(WorkingSet.ImageB.Bmp, 0, WorkingSet.ImageA.Bmp.Height + currentSpace);
        else
          g.DrawImageUnscaled(WorkingSet.ImageB.Bmp, WorkingSet.ImageA.Bmp.Width + currentSpace, 0);
      }
    }
  }

  public class SideBySideToolArgs
  {
    public SideBySideToolArgs() { Orientation = SideBySideOrientation.Vertical; }

    public SideBySideOrientation Orientation { get; set; }
  }

  public enum SideBySideOrientation
  {
    Horizontal,
    Vertical
  }
}