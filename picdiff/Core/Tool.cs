using System;
using System.Drawing;

namespace PicDiff.Core
{
  public abstract class Tool
  {
    private readonly WorkingSet m_workingSet;

    protected Tool(WorkingSet workingSet)
    {
      m_workingSet = workingSet;
      if(workingSet != null) Init(workingSet);
    }

    private void Init(WorkingSet workingSet)
    {
      WorkingSet = workingSet;
    }

    public WorkingSet WorkingSet { get; private set; }
    public abstract void Render(Graphics graphics);
    public abstract Size ResultSize { get; }
  }
}