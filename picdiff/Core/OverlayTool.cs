using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace PicDiff.Core
{
  class OverlayTool : BmpBackedTool<OverlayToolArgs>
  {                                          
    public OverlayTool(WorkingSet workingSet) : base(workingSet) {}

    protected override void UpdateBackingImage(OverlayToolArgs parameters)
    {
      var w = Math.Max(WorkingSet.ImageA.Bmp.Width, WorkingSet.ImageB.Bmp.Width);
      var h = Math.Max(WorkingSet.ImageA.Bmp.Height, WorkingSet.ImageB.Bmp.Height);
      m_mixedBmp = new Bitmap(w, h, PixelFormat.Format32bppArgb);

      float weightB = Parameters.Transparency / 100.0f;

      using (var g = Graphics.FromImage(m_mixedBmp))
      {
        var rcClip = new Rectangle(0, 0, w, h);
        g.DrawImageUnscaled(WorkingSet.ImageA.Bmp, rcClip);

        var colorMatrix = new ColorMatrix();
        colorMatrix.Matrix33 = weightB;
        var imageAttributes = new ImageAttributes();
        imageAttributes = new ImageAttributes();
        imageAttributes.SetColorMatrix(colorMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
        g.DrawImage(WorkingSet.ImageB.Bmp, new Rectangle(0, 0, w, h), 0, 0, w, h, GraphicsUnit.Pixel, imageAttributes);
      }
    }
  }

  public class OverlayToolArgs
  {
    public uint Transparency { get; set; }
  }
}