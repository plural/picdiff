using System.Drawing;

namespace PicDiff.Core
{
  public abstract class BmpBackedTool<ToolParameters> : Tool where ToolParameters : new()
  {
    protected Bitmap m_mixedBmp;
    private ToolParameters m_parameters = new ToolParameters();

    protected BmpBackedTool(WorkingSet workingSet)
      : base(workingSet)
    {
      ApplyParameters(new ToolParameters());
    }

    public override void Render(Graphics graphics)
    {
      graphics.DrawImageUnscaled(m_mixedBmp, 0, 0);
    }

    public override Size ResultSize { get { return m_mixedBmp.Size; } }

    protected ToolParameters Parameters { get { return m_parameters; } }

    protected virtual void OnApplyParameters(ToolParameters parameters)
    {
      //if(parameters.Equals(m_parameters)) return; // todo: compare by value
      m_parameters = parameters;
      UpdateBackingImage(parameters);
    }

    public void ApplyParameters(ToolParameters parameters)
    {
      OnApplyParameters(parameters);
    }

    protected abstract void UpdateBackingImage(ToolParameters parameters);
  }
}