using System.Drawing;
using System.Drawing.Imaging;

namespace PicDiff.Core
{
  public class WorkingSet
  {
    public ImageInfo ImageA { get; set; }
    public ImageInfo ImageB { get; set; }

    public WorkingSet(string pathA, string pathB)
    {
      ImageA = new ImageInfo(pathA);
      ImageB = new ImageInfo(pathB);
    }
  }
}
