using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace PicDiff.Core
{
  class SplitTool : BmpBackedTool<SplitToolArgs>
  {
    const int SPACER = 1;

    public SplitTool(WorkingSet workingSet) : base(workingSet) { }

    protected override void UpdateBackingImage(SplitToolArgs parameters)
    {
      var w = Math.Max(WorkingSet.ImageA.Bmp.Width, WorkingSet.ImageB.Bmp.Width);
      var h = Math.Max(WorkingSet.ImageA.Bmp.Height, WorkingSet.ImageB.Bmp.Height);
      m_mixedBmp = new Bitmap(w, h, PixelFormat.Format32bppArgb);

      using (var g = Graphics.FromImage(m_mixedBmp))
      {
        var wa = (int)(w * Parameters.Ratio / 100f);
        var wb = w - wa;
        wa -= SPACER;
        var rcClip = new Rectangle(0, 0, w, h);

        g.SetClip(new Rectangle(0, 0, wa, h));
        g.DrawImageUnscaled(WorkingSet.ImageA.Bmp, rcClip);

        g.SetClip(new Rectangle(wa + SPACER * 1, 0, wb, h));
        g.DrawImageUnscaled(WorkingSet.ImageB.Bmp, rcClip);
      }
    }
  }

  public class SplitToolArgs
  {
    public SplitToolArgs() { Ratio = 50; }
    public uint Ratio { get; set; }
  }
}