using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace PicDiff.Core
{
  // http://support.microsoft.com/kb/311221

  class DifferenceTool : BmpBackedTool<DifferenceToolArgs>
  {
    public DifferenceTool(WorkingSet workingSet) : base(workingSet) { }

    protected override void UpdateBackingImage(DifferenceToolArgs parameters)
    {
      var w = Math.Max(WorkingSet.ImageA.Bmp.Width, WorkingSet.ImageB.Bmp.Width);
      var h = Math.Max(WorkingSet.ImageA.Bmp.Height, WorkingSet.ImageB.Bmp.Height);
      m_mixedBmp = new Bitmap(w, h, PixelFormat.Format32bppArgb);

      var bmpdataA = WorkingSet.ImageA.Bmp.LockBits(new Rectangle(0, 0, WorkingSet.ImageA.Bmp.Width, WorkingSet.ImageA.Bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
      var bmpdataB = WorkingSet.ImageB.Bmp.LockBits(new Rectangle(0, 0, WorkingSet.ImageB.Bmp.Width, WorkingSet.ImageB.Bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
      var bmpdataM = m_mixedBmp.LockBits(new Rectangle(0, 0, m_mixedBmp.Width, m_mixedBmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);

      Func<uint, uint, uint> fn = BitOps.GetModeFunction(Parameters.Mode);

      unsafe
      {
        uint* bufferA = (uint*) bmpdataA.Scan0;
        uint* bufferB = (uint*) bmpdataB.Scan0;
        uint* bufferM = (uint*) bmpdataM.Scan0;
        for (int idx = 0; idx < w * h; idx += 1)
        {
          *(bufferM + idx) = fn(*(bufferA + idx), *(bufferB + idx));
        }
      }

      WorkingSet.ImageA.Bmp.UnlockBits(bmpdataA);
      WorkingSet.ImageB.Bmp.UnlockBits(bmpdataB);
      m_mixedBmp.UnlockBits(bmpdataM);       
    }
  }

  public class DifferenceToolArgs
  {
    public DifferenceMode Mode { get; set; }
    public bool TransparentBackground { get; set; }
  }

  public enum DifferenceMode
  {
    XorNot,
    Xor,
    Distance,
    MaskSame,
    MaskDifferent
  }

  static class BitOps
  {
    public static uint XOR_NOT(uint a, uint b) { return a ^ ~b; }
    public static uint Distance(uint a, uint b)
    {
      var diff = a - b;
      var l = (diff & 0xFF + ((diff & 0xFF00) >> 8) + ((diff & 0xFF0000) >> 16)) / 3;
      return 0xFF000000 | ((l << 16) + (l << 8) + l);        
    }
    public static uint XOR(uint a, uint b) { return 0xFF000000 | (a ^ b); }
    public static uint MASK_SAME(uint a, uint b) { return (a ^ b) == 0 ? 0xFF000000 : 0xFFFFFFFF; }
    public static uint MASK_DIFFERENT(uint a, uint b) { return ~MASK_SAME(a,b) | 0xFF000000; }

    public static Func<uint, uint, uint> GetModeFunction(DifferenceMode mode)
    {
      switch (mode)
      {
        case DifferenceMode.XorNot:
          return BitOps.XOR_NOT;
        case DifferenceMode.Xor:
          return BitOps.XOR;
        case DifferenceMode.Distance:
          return BitOps.Distance;
        case DifferenceMode.MaskSame:
          return BitOps.MASK_SAME;
        case DifferenceMode.MaskDifferent:
          return BitOps.MASK_DIFFERENT;
        default:
          throw new ArgumentOutOfRangeException("mode");
      }
    }

  }


}