PicDiff
=======

A Windows/.NET tool for image comparison.

Supports multiple comparison modes:

* side by side - images are shown one next to the other
* overlay - one on top of the other, with custom blending
* split - one on top of the other, split and showing parts of both
* difference - one on top of the other, showing only the differences

Following image formats are supported: JPG, PNG, BMP, GIFF, TIFF.

Requirements
------------

.NET Framework 2.0
